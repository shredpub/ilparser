#!/usr/env python

"""
Parser Entry Point

Base class for Dependency Parser
Decides whether to plot parse tree generated by the dependency-parser
"""

__version__    = "1.0"
__license__    = "MIT"
__author__     = ["Riyaz Ahmad", "Irshad Ahmad"]
__maintainer__ = "Irshad Ahmad"
__email__      = "riyaz.bhat@research.iiit.ac.in"

import re
import os
import sys
import shutil
import warnings
from string import punctuation as punct
from collections import namedtuple as nt
from collections import defaultdict as dfd

import json
import pydot
import urllib
import urllib3
import numpy as np

from ilparser import ILPARSER
from rawtoconll import toConll
from converter_indic import wxConvert
from pseudo_projectivity import deprojectivize, adjacencyMatrix

class ilparser():
    def __init__(self, out_dir=None, plot=False, beamWidth=1, conll=False, script_convertor=False, lang='hin'):
        self.count = 0
        self.plot = plot
	self.conll = conll
	self.beamWidth = beamWidth
        rootDir = os.path.realpath(__file__).rpartition('/')[0]
        if self.plot:
            self.outPath = os.path.expanduser(out_dir) if out_dir else '%s/output-trees' %(os.path.expanduser("~"))
            if os.path.exists(os.path.abspath(self.outPath)):
                shutil.rmtree(os.path.abspath(self.outPath))
                os.mkdir(self.outPath)
            else:
                os.mkdir(self.outPath)

        self.model = np.load('%s/models/pmodel_%s_psuedoprojective.npy' % (rootDir, lang))
	self.convertor = script_convertor
        self.parser = ILPARSER(self.model, beamWidth=self.beamWidth)

    def ilmtApi(self, first, last, text):
        pool = urllib3.PoolManager()
	url = 'http://api.ilmt.iiit.ac.in/hin/pan/%s/%s' % (first, last)
        method = 'POST'
        headers = {'Content-Type':'application/x-www-form-urlencoded', 'charset':'UTF-8'}
        data = pool.urlopen(method, url, headers = headers, body = text).data
        return json.loads(data)
    
    def dependencyGraph(self, conll_text):
        for node in [0] + conll_text + [-1]:
            leaf = nt('leaf', \
			['id','form','lemma','ctag','tag','features','parent','pparent','gdrel', \
				'pdrel', 'visit','leftChildren','rightChildren'] )
	    if node in [0, -1]:
		leaf.id,leaf.form,leaf.lemma,leaf.ctag,leaf.tag,leaf.features,\
			leaf.parent,leaf.pparent,leaf.gdrel,leaf.pdrel, leaf.visit, leaf.leftChildren,leaf.rightChildren =\
					0, 'ROOT_F', 'ROOT_L', 'ROOT_C', 'ROOT_P', dfd(str), -1, -1, '_', '_', False, [], []
		yield leaf
	    else:
	        leaf.id,leaf.form,leaf.lemma,leaf.ctag,leaf.tag,feats,leaf.parent,leaf.gdrel, _ ,_ = node.strip().split("\t")
	        features = dfd(str)
	        for feature in feats.split("|"): 
	            try:
	                features[feature.split("-")[0]] = feature.split("-")[1]
	            except IndexError: features[feature.split("-")[0]] = ''
	        leaf.features = features
	        leaf.id = int(leaf.id)
	        leaf.pdrel = "_"
	        leaf.gdrel = "_"
	        leaf.visit = False
	        leaf.parent = -1 #if leaf.parent == "_" else int(leaf.parent)
	        leaf.pparent = leaf.parent
                leaf.leftChildren = []
                leaf.rightChildren = []
	        yield leaf
    
    def BFSPlot(self, data, adjMatrix, root):
        """Breadth-first search over labelled arcs to generate a dependency tree for visualisation via pydot."""
        graph = pydot.Dot(graph_type='graph', splines='polyline')
        pNode = pydot.Node(root, label=data[root].form, shape='plaintext')
        graph.add_node(pNode)
        queue = [(root, pNode)]
        while queue:
            node,pNode = queue.pop(0)
            adjList = np.nonzero(adjMatrix[node])[0]
            for child in adjList:
                cNode = pydot.Node(child, label=data[child].form, shape='plaintext')
                graph.add_node(cNode)
                edge = pydot.Edge(pNode, cNode, label=data[child].gdrel.replace("%",''), fontsize="8.0", fontcolor='blue')
                graph.add_edge(edge)
                queue.append((child, cNode))

        return graph
    
    def getParse(self, sentences, sflag=True):
        iterable = True
        out_sentences = list()
        if sflag and (not hasattr(sentences, '__iter__')):
           iterable = False
           sentences = sentences.split("\n\n")
    
        for sentence in sentences:
	    if self.conll:
		conllSentence = sentence
	    else:
            	tillHeadComp = self.ilmtApi('1', '9', "input=%s" % (sentence))
            	payload = "keep=true&input=" + urllib.quote(tillHeadComp['computehead-9'])
            	vibComputedSSF = self.ilmtApi('10', '10', payload)["computevibhakti-10"] #NOTE PSPs and Auxillaries are retained.
	    	conllSentence = "\n".join(list(toConll(vibComputedSSF)))
	    	conllSentence = self.convertor.convert(conllSentence)
            dp_graph = self.dependencyGraph(conllSentence.split("\n"))
	    beam = self.parser.parse(list(dp_graph))
	    for c_id, configuration in enumerate(beam):
	        nodes = configuration.nodes[1:-1]
            	nodes = deprojectivize(nodes)
                parse_out = list()
                for node in nodes:
                   if node.parent == 0: root = node.id - 1
                   feats = "|".join(["%s-%s" % (feat,value) for feat,value in node.features.items()])
                   parse_out.append("\t".join((str(node.id), node.form, node.lemma, node.ctag, node.tag, feats,
                       '0' if str(node.parent) == '-1' else str(node.parent), 'root' if node.gdrel == "_" \
	           	else node.gdrel.replace("%", ''), "_", "_")))
                out_sentences.append('\n'.join(parse_out))
                if self.plot:
                   self.count += 1
                   dp_graph = adjacencyMatrix(nodes)
                   graph = self.BFSPlot(nodes, dp_graph, root)
                   graph.write_png("%s/Sentence-%s.png" % (self.outPath, self.count))
          
        return ['\n\n'.join(out_sentences)]
