#! /usr/bin/env python

import sys
import os.path
import argparse

from .DP import ilparser
from converter_indic import wxConvert

__name__       = "ilparser"
__doc__        = "ilparser: Parser for Indian Languages"
__author__     = ["Riyaz Ahmad", "Irshad Ahmad"]
__version__    = "1.5.0"
__license__    = "GPU"
__maintainer__ = "Irshad Ahmad"
__email__      = "riyaz.bhat@research.iiit.ac.in"
__all__        = ["ilparser", "pseudo_projectivity", "feature_extractor", "DP"]

def main():
    # parse command line arguments 
    languages = ['ben', 'hin', 'kas', 'tel', 'urd']
    lang_help = "<%s>" % ("|".join(languages))
    parser = argparse.ArgumentParser(prog="ilparser", description="Parser for Indian Languages")
    parser.add_argument('--v', action="version", version="%(prog)s 1.0")
    parser.add_argument('--input', metavar='input', dest="INFILE", type=argparse.FileType('r'), required=True, help="<input-file>")
    parser.add_argument('--output', metavar='output', dest="OUTFILE", type=argparse.FileType('w'), default=sys.stdout, help="<output-file>")
    parser.add_argument('--lang', metavar='language', dest="lang", default='hin', choices=languages, help=lang_help)
    parser.add_argument('--plot', action='store_true', dest="plot", help="set this flag to plot output parse trees")
    parser.add_argument('--dir', metavar='directory', dest="out_dir", help="directory for plotted parse trees")
    parser.add_argument('--conll', action='store_true', dest="conll", help="set the flag about the input format")
    parser.add_argument('--beams', type=int, default=1, dest="beams", help="number for beams for beam search decoding")
    args = parser.parse_args()

    
    sentences = args.INFILE.read().split('\n\n') if args.conll else args.INFILE.read().split('\n')
    args.INFILE.close()

    sys.stderr.write("Loading Models ....\n\n")
    convertor = wxConvert(order='wx2utf', lang=args.lang, format_='conll')
    parser = ilparser(out_dir=args.out_dir,plot=args.plot,conll=args.conll,script_convertor=convertor,beamWidth=args.beams,lang=args.lang)
    for sentence in sentences:
	if not sentence.strip():continue
	out_parse = parser.getParse([sentence], sflag=False)
	if out_parse:
        	args.OUTFILE.write("%s\n\n" %(out_parse[0]))
    args.OUTFILE.close()
