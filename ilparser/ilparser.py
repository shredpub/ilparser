#!/usr/env python
# Copyright Riyaz Ahmad 2015 - present
#
# This file is part of IL-Parser library.
# 
# IL-Parser library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# IL-Parser Library is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#        GNU General Public License for more details.
# 
#        You should have received a copy of the GNU General Public License
#        along with IL-Parser Library.  If not, see <http://www.gnu.org/licenses/>.
#

# Program for dependency parsing  
#
# @author Riyaz Ahmad
#

import re
import copy
import numpy as np

from collections import namedtuple as nt

from arc_eager import arceager
from feature_extractor import Templates

class Configuration:
    def __init__(self, nodes=[]):
        self.stack = list()
        self.score = 0.0
        self.b0 = 1
        self.nodes = nodes
	self.transitions = list()

class BeamItem:
    def __init__(self, number, score, action, label):
        self.number = number
        self.score = score
        self.action = action
        self.label = label

class ILPARSER(object):
    
    def __init__(self, model, beamWidth=1):
        self.beamWidth = beamWidth
        self.coeff,self.label_map = model
	self.EagerParser = arceager()
	self.Template = Templates()

    def createBeamItems(self, beam):
	beamItems = [] 
	for b in xrange(len(beam)):
	    configuration = beam[b]
	    prevScore = configuration.score
            features = self.Template.ExtendedZhang(configuration.nodes, configuration.stack, configuration.b0)
            validTransitions = self.EagerParser.getValidTransitions(configuration)
	    predictions = [sum(_class.get(feature, 0) for feature in features) for _class in self.coeff]	
            for action, score in enumerate(predictions):
                if self.label_map[action][0] in validTransitions:
                    next_transition_id, label = self.label_map[action]
                    next_transition = validTransitions[next_transition_id]
                    beamItems.append(BeamItem(b, prevScore + score, next_transition, label))
                    if len(beamItems) > self.beamWidth:
			del beamItems[min(range(len(beamItems)), key=lambda x: beamItems[x].score)]
	return beamItems

    def clone(self, configuration):
	newConfiguration = Configuration()
	newConfiguration.stack = configuration.stack[:]
	newConfiguration.score = configuration.score
	newConfiguration.b0 = configuration.b0
	newConfiguration.transitions = configuration.transitions[:]
	newConfiguration.nodes = []
	for node in configuration.nodes:
        	leaf = nt('leaf', \
			['id','form','lemma','ctag','tag','features','parent','pparent','gdrel', \
				'pdrel', 'visit','leftChildren','rightChildren'] )
		leaf.id, leaf.form, leaf.lemma, leaf.ctag, leaf.tag, leaf.features,\
                        leaf.parent, leaf.pparent, leaf.gdrel, leaf.pdrel, leaf.visit, leaf.leftChildren, leaf.rightChildren = \
			node.id, node.form, node.lemma, node.ctag, node.tag, node.features.copy(), node.parent, node.pparent, \
				node.gdrel, node.pdrel, node.visit, node.leftChildren[:], node.rightChildren[:]
		
		newConfiguration.nodes.append(leaf)
	return newConfiguration
		
    def parse(self, nodes):
        """
        Parses sentence incrementally till all the words are consumed and only root node is left.
        """
	beam = [Configuration(nodes)]
        while not self.EagerParser.isFinalState(beam):
	    beamItems = self.createBeamItems(beam)
            newBeam = list()
            for beamItem in beamItems:
	    	b = beamItem.number
	     	score = beamItem.score
	     	label = beamItem.label
	    	action = beamItem.action
		currentConfiguration = beam[b]
	    	newConfiguration = self.clone(currentConfiguration)
            	action(newConfiguration, label)
                newConfiguration.score = score
		transition_sequence = "%s:%s:%s" % (re.search("SHIFT|REDUCE|RIGHTARC|LEFTARC", str(action)).group(0),
					round(score,1), label if label else '')
		newConfiguration.transitions.append(transition_sequence.strip(":"))
	    	newBeam.append(newConfiguration)
            beam = newBeam 
        return sorted(beam, key=lambda b: b.score, reverse=True)
