#!/usr/bin/python -*- coding:utf-8 -*-

__Author__ = "Riyaz Ahmad Bhat"
__Email__ = "riyaz.ah.bhat@gmail.com"


import re

def toConll (sentence):

	chunkIds = list()
	chunkId_,chunkHead_,lemma_,cat_,gen_,num_,per_,case_,vib_,tam_ = [None]*10

	nodeId_ = 0
	for line in sentence.strip().split("\n"):
		if line.split("\t")[0].isdigit():
			assert len(line.split("\t")) == 4 # no need to process trash! FIXME
			chunkId_ = line.split("\t")[2]
			chunkIds.append(chunkId_)
			count_ = chunkIds.count(chunkId_)
			chunkId_ = "%s%s" % (chunkId_, count_) if count_ > 1 else chunkId_
			keyValue_pairs = FSPairs(line.split("\t")[3][4:-1])
			for key,value in keyValue_pairs.items():
				if key == "af": lemma_,cat_,gen_,num_,per_,case_,vib_,tam_ = morphFeatures (value)
				elif key == "head": chunkHead_ = re.sub("'|\"",'',value)
				else: pass
		elif line.split("\t")[0].replace(".",'',1).isdigit():
			nodeId_ += 1
			attributeValue_pairs = FSPairs(line.split("\t")[3][4:-1])
			wordForm_, posTag_ = line.split("\t")[1:3]
                        
			if attributeValue_pairs.get('name','').strip("'\"") == chunkHead_:# NOTE head word of the chunk
				features_ = "gen-%s|num-%s|pers-%s|case-%s|vib-%s|tam-%s|chunkId-%s" \
									% (gen_,num_,per_,case_,vib_,tam_,chunkId_)
				yield "\t".join((str(nodeId_),wordForm_,lemma_,cat_,posTag_,features_,"_", "_","_","_"))
			else:
				keyValue_pairs = FSPairs(line.split("\t")[3][4:-1])
				llemma_,lcat_,lgen_,lnum_,lper_,lcase_,lvib_,ltam_ = morphFeatures (keyValue_pairs.get("af", ''))
				lfeatures_ = "gen-%s|num-%s|pers-%s|case-%s|vib-%s|tam-%s|chunkId-%s" \
									% (lgen_,lnum_,lper_,lcase_,lvib_,ltam_,chunkId_)
				yield "\t".join((str(nodeId_),wordForm_,llemma_,lcat_,posTag_,lfeatures_,"_", "_","_","_"))
		else:
			chunkId_,chunkHead_,lemma_,cat_,gen_,num_,per_,case_,vib_,tam_ = [None]*10
		
def FSPairs (FS) :

	feats = dict()
	for feat in FS.split():
		if "=" not in feat:continue
		feat = re.sub("af='+","af='",feat.replace("dmrel=",'drel='))
		attribute,value = feat.split("=")
		feats[attribute] = value

	return feats

def morphFeatures (AF):
	"LEMMA,CAT,GEN,NUM,PER,CASE,VIB,TAM"
	assert len(AF[:-1].split(",")) == 8 # no need to process trash! FIXME
	lemma_,cat_,gen_,num_,per_,case_,vib_,tam_ = AF.split(",")

	if len(lemma_.decode("utf-8")) > 1:
		lemma_ = lemma_.strip("'")
	else:
		lemma_ = lemma_
	
	return lemma_,cat_,gen_,num_,per_,case_,vib_,tam_.strip("'")

'''	
if __name__ == "__main__":

	import re
	import sys

	inputFile = open(sys.argv[1]).read()
	
	sentences = re.findall("<Sentence id=.*?>(.*?)</Sentence>",inputFile, re.S)
	for idx,sentence in enumerate(sentences):
		for csentence in getAnnotations(sentence):
			print csentence
		print
		#sys.exit()
'''
